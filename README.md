# 20191015-ShengXu-NYCSchools


Finished:
1. Use CoreData to cache school info for offline use
2. Table view to show the list of NYC high schools
3. Map View to show schools 
4. Detail view to show info for a selected high school


                    Tableview  <--->  Mapview
                                ^  
                                |
                                v
                           Detailview

Todo:
1. drag down table view to refresh school list
2. search function
3. query school list by pages
4. Show more info in detail view
5. make ui look better
6. Links to phone call, email & website etc
7. Share

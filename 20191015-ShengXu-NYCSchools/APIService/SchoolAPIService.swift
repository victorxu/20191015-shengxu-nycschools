//
//  SchoolAPIService.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/16/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//  Handle network api call to get school list and school' sat
//

import Foundation
import UIKit


class SchoolAPIService: NSObject {
    let apiToken = "9muPcMjU3DumB0IuEQ7FTV5Qj"
    let schoolInfoEndPoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let schoolSatEndPoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    // TODO: add pagination support
    func getSchoolInfo(completion: @escaping (Result<[[String: String]]>) -> Void) {
        let urlWithToken = String("\(schoolInfoEndPoint)?$$app_token=\(apiToken)")
        guard let url = URL(string: urlWithToken) else {
            return completion(.Error("Invalid URL, we can't fetch school list"))
        }
        
        executeDataTask(url: url, errMsg: "There are no schools found", completion: completion)
    }
    
    func getSchoolSat(dbn: String, completion: @escaping (Result<[[String: String]]>) -> Void) {
        let urlWithDbn = String("\(schoolSatEndPoint)?dbn=\(dbn)&&$$app_token=\(apiToken)")
        guard let url = URL(string: urlWithDbn) else {
            return completion(.Error("Invalid URL, we can't fetch school list"))
        }
        
        executeDataTask(url: url, errMsg: "There are no school sat found", completion: completion)
    }
    
    private func executeDataTask(url : URL, errMsg: String, completion: @escaping (Result<[[String: String]]>) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                return completion(.Error(error!.localizedDescription))
            }
            
            guard let data = data else {
                return completion(.Error(error?.localizedDescription ?? errMsg))
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [[String: String]] {
                    if json.isEmpty {
                        return completion(.Error(error?.localizedDescription ?? errMsg))
                    }
                    
                    DispatchQueue.main.async {
                        completion(.Success(json))
                    }
                }
            }catch let err {
                return completion(.Error(err.localizedDescription))
            }
        }.resume()
    }
}

enum Result<T> {
    case Success(T)
    case Error(String)
}

//
//  School+CoreDataProperties.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/16/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//

import Foundation
import CoreData


extension School {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<School> {
        return NSFetchRequest<School>(entityName: "School")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var school_name: String?
    @NSManaged public var overview_paragraph: String?
    @NSManaged public var phone_number: String?
    @NSManaged public var school_email: String?
    @NSManaged public var fax_number: String?
    @NSManaged public var website: String?
    @NSManaged public var subway: String?
    @NSManaged public var bus: String?
    @NSManaged public var finalgrades: String?
    @NSManaged public var total_students: String?
    @NSManaged public var primary_address_line_1: String?
    @NSManaged public var city: String?
    @NSManaged public var zip: String?
    @NSManaged public var state_code: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?

}

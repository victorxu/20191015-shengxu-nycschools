//
//  Sat+CoreDataProperties.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/16/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//

import Foundation
import CoreData


extension Sat {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sat> {
        return NSFetchRequest<Sat>(entityName: "Sat")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var num_of_sat_test_takers: String?
    @NSManaged public var sat_critical_reading_avg_score: String?
    @NSManaged public var sat_math_avg_score: String?
    @NSManaged public var sat_writing_avg_score: String?

}

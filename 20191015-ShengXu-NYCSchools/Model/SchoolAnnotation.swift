//
//  SchoolAnnotation.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/17/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//

import UIKit
import MapKit

@objcMembers class SchoolAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    var school: School
    // school name
    var title: String?
    var subtitle: String?
    
    // bus info
    var bus: String?
    
    // subway info
    var subway: String?
    
    init(coordinate: CLLocationCoordinate2D, school: School, title: String?, subtitle: String?, bus: String?, subway: String?) {
        self.coordinate = coordinate
        self.school = school
        self.title = title
        self.subtitle = subtitle
        self.bus = bus
        self.subway = subway
        
        super.init()
    }
}

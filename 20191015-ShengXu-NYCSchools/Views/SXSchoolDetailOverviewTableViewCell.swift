//
//  SXSchoolDetailOverviewTableViewCell.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/16/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//

import UIKit

class SXSchoolDetailOverviewTableViewCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbGrades: UILabel!
    @IBOutlet weak var lbTotalStudents: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellWith(school: School) {
        lbName.text = school.school_name
        lbDesc.text = school.overview_paragraph ?? ""
        lbPhone.text = String("Phone: \(school.phone_number ?? "")")
        lbEmail.text = String("Fax: \(school.fax_number ?? "")")
        lbAddress.text = String("\(school.primary_address_line_1 ?? ""), \(school.city ?? "") \(school.state_code ?? "") \(school.zip ?? "")")
        lbGrades.text = String("Grades: \(school.finalgrades ?? "6-12")")
        lbTotalStudents.text = String("Total students: \(school.total_students ?? "")")
    }
}

//
//  SXSchoolTableViewCell.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/15/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//

import UIKit
import CoreData

class SXSchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    @IBOutlet weak var lbGrade: UILabel!
    
    @IBOutlet weak var lbTotalStudent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellWith(school: School) {
        lbName.text = school.school_name
        lbAddress.text = String("\(school.primary_address_line_1 ?? ""), \(school.city ?? "") \(school.state_code ?? "") \(school.zip ?? "")")
        lbGrade.text = String("Grades: \(school.finalgrades ?? "6-12")")
        lbTotalStudent.text = String("Total students: \(school.total_students ?? "")")
    }

}

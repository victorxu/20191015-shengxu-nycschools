//
//  SXSchoolDetailSatTableViewCell.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/16/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//

import UIKit

class SXSchoolDetailSatTableViewCell: UITableViewCell {

    @IBOutlet weak var lbTestTakers: UILabel!
    @IBOutlet weak var lbReadingScore: UILabel!
    @IBOutlet weak var lbWritingScore: UILabel!
    @IBOutlet weak var lbMathScore: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCellWith(sat: Sat?, msg: String) {
        if let sat = sat {
            lbTestTakers.text = String("Number of sat test takers: \(sat.num_of_sat_test_takers ?? "")")
            lbReadingScore.text = String("Sat critical reading avg score: \(sat.sat_critical_reading_avg_score ?? "")")
            lbWritingScore.text = String("Sat writing avg score: \(sat.sat_writing_avg_score ?? "")")
            lbMathScore.text = String("Sat math avg score: \(sat.sat_math_avg_score ?? "")")
        }
        else {
            lbTestTakers.text = msg
            lbReadingScore.text = " "
            lbWritingScore.text = " "
            lbMathScore.text = " "
        }
    }
}

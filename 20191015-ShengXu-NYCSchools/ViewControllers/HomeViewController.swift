//
//  ViewController.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/15/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//  Home screen
//

import UIKit
import MapKit

class HomeViewController: UIViewController {
    static let VIEW_TRANSITION_DURATION = 0.25
    
    enum SchoolDisplayMode : Int {
        case listMode = 0
        case MapMode
    }
    
    private var schoolListViewController : SXSchoolListViewController!
    private var schoolMapViewController : SXSchoolMapViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // add "List" and "Map" segmented control to switch between two modes
        let segmentControl = UISegmentedControl(items: ["List", "Map"])
        segmentControl.addTarget(self, action: #selector(onSchoolDisplayModeChange(_:)), for: .valueChanged)
        segmentControl.selectedSegmentIndex = 0
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: segmentControl)
        
        self.title = "NYCSchools"
        
        // set list view controller as default vc
        schoolListViewController = (storyboard?.instantiateViewController(withIdentifier: "SXSchoolListViewController") as! SXSchoolListViewController)
        
        addChild(schoolListViewController)
        self.view.addSubview(schoolListViewController.view)
    }

    
    // change between List mode and Map mode with animation
    @objc func onSchoolDisplayModeChange(_ sender: UISegmentedControl) {
        let curMode = SchoolDisplayMode(rawValue: sender.selectedSegmentIndex)
        switch curMode {
        case .listMode:
            switchControllers(from: schoolMapViewController!, to: schoolListViewController, fromLeftToRight: true)
            
        default:
            // lazy loading
            if schoolMapViewController == nil {
                schoolMapViewController = (storyboard?.instantiateViewController(withIdentifier: "SXSchoolMapViewController") as! SXSchoolMapViewController)
            }

            switchControllers(from: schoolListViewController, to: schoolMapViewController!, fromLeftToRight: false)
            if schoolListViewController.schoolAnnotations.count > 0 {
                schoolMapViewController!.updateSchoolAnnotations(schoolListViewController!.schoolAnnotations)
            }
        }
    }
    
    
    private func switchControllers(from: UIViewController, to: UIViewController, fromLeftToRight: Bool) {
        self.view.addSubview(to.view)
  
        let width = self.view.frame.size.width;
        
        if fromLeftToRight {
            to.view.frame.origin.x = -width
        }
        else {
            to.view.frame.origin.x = width
        }
        
        UIView.animate(withDuration: HomeViewController.VIEW_TRANSITION_DURATION, delay: 0, options: .curveEaseIn, animations: { () in
            let fromView = from.view
            let toView = to.view
            
            if fromLeftToRight {
                fromView?.frame.origin.x = width
            }
            else {
                fromView?.frame.origin.x = -width
            }
            
            toView?.frame.origin.x = 0
        }) { [weak self] (completed) in
            if completed {
                if let self = self {
                    from.view.removeFromSuperview()
                    from.removeFromParent()
                    self.addChild(to)
                }
            }
        }
    }
    
    private func showAlert(msg: String) {
        let alertVC = UIAlertController(title: "error", message: msg, preferredStyle: .alert)
        let alertDone = UIAlertAction(title: "Done", style: .cancel, handler: nil)
        alertVC.addAction(alertDone)
        present(alertVC, animated: true, completion: nil)
    }
}


//
//  SXSchoolMapViewController.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/17/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//
//  Shows NYC High schools in a map
//

import UIKit
import MapKit

class SXSchoolMapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    var schoolAnnotations = [SchoolAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.register(MKPinAnnotationView.self, forAnnotationViewWithReuseIdentifier: "schoolAnnotionView")
        mapView.delegate = self
    }

    func updateSchoolAnnotations(_ annotations: [SchoolAnnotation]) {
        if annotations.count != schoolAnnotations.count {
            self.schoolAnnotations = annotations;
        
            mapView.addAnnotations(schoolAnnotations)
            mapView.showAnnotations(schoolAnnotations, animated: true)
        }
    }
}

extension SXSchoolMapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pav = mapView.dequeueReusableAnnotationView(withIdentifier: "schoolAnnotionView", for: annotation)
     
        pav.canShowCallout = true
        pav.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
        return pav
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if let satVC = storyboard?.instantiateViewController(withIdentifier: "SXSchoolDetailViewController") as? SXSchoolDetailViewController {
            let annotation = view.annotation as! SchoolAnnotation
            satVC.school = annotation.school
            //self.present(satVC, animated: true, completion: nil)
            navigationController?.pushViewController(satVC, animated: true)
        }
    }
}


//
//  SXSchoolDetailViewController.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/15/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//
//  Shows detail info including SAT of a NYC High school
//

import UIKit
import CoreData

class SXSchoolDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var school : School?
    var sat : Sat?
    let app = UIApplication.shared.delegate as! AppDelegate
    let apiService = SchoolAPIService()
    
    private enum LoadingState {
        case loading, finished, empty, error
    }
    
    private var state: LoadingState = .finished
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Detail"
        setupTableView()
        reload()
    }
    
    private func fetchUserSat() {
        let context = app.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Sat>(entityName: "Sat")
        fetchRequest.predicate = NSPredicate(format: "dbn == %@", self.school!.dbn!)
        
        do {
            let sats = try context.fetch(fetchRequest)
            self.sat = sats.first
        } catch let error as NSError {
            print(error)
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = self
    }
    
    private func reload() {
        fetchUserSat()
        tableView.reloadData()
        
        if sat == nil{
            state = .loading
            apiService.getSchoolSat(dbn: school!.dbn!) { [weak self] (result) in
                switch result {
                    case .Success(let response):
                        self?.sat = self?.createSatEntity(dict: response.first!) as? Sat
                        if self?.sat == nil {
                            self?.state = .empty
                        }
                        else {
                            self?.state = .finished
                        }
                        self?.app.saveContext()
                    
                    case .Error(let error):
                        self?.state = .error
                        print(error)
                }
                DispatchQueue.main.async {
                    self?.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)
                }
            }
        }
    }

    private func createSatEntity(dict: [String: String]) -> NSManagedObject? {
        let context = app.persistentContainer.viewContext
        if let satEntity = NSEntityDescription.insertNewObject(forEntityName: "Sat", into: context) as? Sat {
            satEntity.dbn = dict["dbn"]
            satEntity.num_of_sat_test_takers = dict["num_of_sat_test_takers"]
            satEntity.sat_critical_reading_avg_score = dict["sat_critical_reading_avg_score"]
            satEntity.sat_writing_avg_score = dict["sat_writing_avg_score"]
            satEntity.sat_math_avg_score = dict["sat_math_avg_score"]

            return satEntity
        }
        
        return nil
    }
}

extension SXSchoolDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "General"
        default:
            return "Sat"
        }
    }
}

extension SXSchoolDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SXSchoolDetailOverviewTableViewCell", for: indexPath) as? SXSchoolDetailOverviewTableViewCell else { fatalError("Cannot create new cell") }
            
            cell.setCellWith(school: school!)
            
            return cell
        }
        else {
        //else if(indexPath.section == 1) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SXSchoolDetailSatTableViewCell", for: indexPath) as? SXSchoolDetailSatTableViewCell else { fatalError("Cannot create new cell") }
            
            var errMsg : String?
            switch state {
            case .loading:
                errMsg = "Loading"
            case .error:
                errMsg = "Error"
            default:
                errMsg = " "
            }
                
            cell.setCellWith(sat: sat, msg: errMsg!)
            
            return cell
        }
        /*else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SXSchoolDetailDirectionTableViewCell", for: indexPath) as! SXSchoolDetailDirectionTableViewCell
            
            //cell.setCellWith(school: school)
            
            return cell
        }*/
    }
}


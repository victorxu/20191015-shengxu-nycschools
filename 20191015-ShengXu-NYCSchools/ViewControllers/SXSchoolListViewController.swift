//
//  SXSchoolListViewController.swift
//  20191015-ShengXu-NYCSchools
//
//  Created by victor on 10/15/19.
//  Copyright © 2019 Sheng Xu. All rights reserved.
//
//  Shows list of NYC High schools
//
/*
    Todo:
    1. drag down table view to refresh school list
    2. search function
    3. query school list by pages
    4. make ui look better

*/
import UIKit
import CoreData
import CoreLocation


class SXSchoolListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    let app = UIApplication.shared.delegate as! AppDelegate
    let apiService = SchoolAPIService()
    
    lazy var fetchedResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: School.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "school_name", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: app.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    var schoolAnnotations = [SchoolAnnotation]()
    
    private enum LoadingState {
        case loading, finished, empty, error
    }
    
    private var state: LoadingState = .empty {
        didSet {
            switch state {
            case .loading:
                tableView.isHidden = true
                statusLabel.text = "Loading"
                activityIndicator.startAnimating()
            case .finished:
                tableView.isHidden = false
                statusLabel.text = nil
                activityIndicator.stopAnimating()
            case .empty:
                tableView.isHidden = true
                statusLabel.text = "No schools found"
                activityIndicator.stopAnimating()
            case .error:
                tableView.isHidden = true
                statusLabel.text = "Something went wrong"
                activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        reload()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 84
        tableView.rowHeight = UITableView.automaticDimension
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
        tableView.tableHeaderView = searchBar
    }
    
    private func reload() {
        state = .loading
        
        do {
            try fetchedResultController.performFetch()
            let count = fetchedResultController.sections?[0].numberOfObjects
            
            if count == 0 {
                apiService.getSchoolInfo{ [weak self] (result) in
                    switch result {
                        case .Success(let response):
                            //self?.clearData()
                            self?.saveInCoreData(response)
                            self?.generateSchoolAnnotations()
                            self?.state = .finished
                            self?.tableView.reloadData()
                        
                        case .Error(let error):
                            self?.state = .error
                            print(error)
                    }
                }
            }
            else {
                generateSchoolAnnotations()
                state = .finished
            }
        } catch let error  {
            state = .error
            print("ERROR: \(error)")
        }
    }
    
    private func createSchoolEntity(dict: [String: String]) -> NSManagedObject? {
        let context = app.persistentContainer.viewContext
        if let schoolEntity = NSEntityDescription.insertNewObject(forEntityName: "School", into: context) as? School {
            schoolEntity.dbn = dict["dbn"]
            schoolEntity.school_name = dict["school_name"]
            schoolEntity.overview_paragraph = dict["overview_paragraph"]
            schoolEntity.phone_number = dict["phone_number"]
            schoolEntity.fax_number = dict["fax_number"]
            schoolEntity.school_email = dict["school_email"]
            schoolEntity.website = dict["website"]
            
            schoolEntity.subway = dict["subway"]
            schoolEntity.bus = dict["bus"]
            
            schoolEntity.finalgrades = dict["finalgrades"]
            schoolEntity.total_students = dict["total_students"]
            
            schoolEntity.primary_address_line_1 = dict["primary_address_line_1"]
            schoolEntity.city = dict["city"]
            schoolEntity.zip = dict["zip"]
            schoolEntity.state_code = dict["state_code"]
            schoolEntity.latitude = dict["latitude"]
            schoolEntity.longitude = dict["longitude"]
            
            return schoolEntity
        }
        
        return nil
    }

    private func saveInCoreData(_ data: [[String: String]]) {
        _ = data.map{self.createSchoolEntity(dict: $0)}
        
        do {
            let context = app.persistentContainer.viewContext
            
            try context.save()
        } catch let error {
            print(error)
        }
    }
    
    private func clearData() {
        do {
            let context = app.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: School.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                app.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
    private func generateSchoolAnnotations() {
        guard let count = fetchedResultController.sections?[0].numberOfObjects else { return }
        
        for i in 0..<count {
            if let school = fetchedResultController.object(at: IndexPath(row: i, section: 0)) as? School {
                if let strLat = school.latitude, let lat = Double(strLat), let strLot = school.longitude, let lot = Double(strLot) {
                    let loc2D = CLLocationCoordinate2D(latitude: lat, longitude: lot)
                    
                    let schoolAnnotation = SchoolAnnotation(coordinate: loc2D, school: school, title: school.school_name, subtitle: school.primary_address_line_1, bus: school.bus, subway: school.subway)
                    
                    schoolAnnotations.append(schoolAnnotation)
                }
            }
        }
    }
}

extension SXSchoolListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = fetchedResultController.sections?.first?.numberOfObjects {
            return count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SXSchoolTableViewCell", for: indexPath) as? SXSchoolTableViewCell else { fatalError("Cannot create new cell") }
        
        if let school = fetchedResultController.object(at: indexPath) as? School {
            cell.setCellWith(school: school)
        }
        return cell
    }
}

extension SXSchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let school = fetchedResultController.object(at: indexPath) as? School {
            if let satVC = storyboard?.instantiateViewController(withIdentifier: "SXSchoolDetailViewController") as? SXSchoolDetailViewController {
                satVC.school = school
                //self.present(satVC, animated: true, completion: nil)
                navigationController?.pushViewController(satVC, animated: true)
            }
        }
    }
}


extension SXSchoolListViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
}
